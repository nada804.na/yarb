//const oracledb = require('oracledb');
const logger=require('../util/logger.util');


function executeQurey(statement, opts = {}) {
  return new Promise(async (resolve, reject) => {
    let conn;
    opts.outFormat = oracledb.OBJECT;
    binds=[];
    //opts.autoCommit = true;

    try {
      conn = await oracledb.getConnection();
      const result = await conn.execute(statement, binds, opts);
      resolve(result);
    } catch (err) {
      reject(err);
    } finally {
      if (conn) { // conn assignment worked, need to close
        try {
          await conn.close();
        } catch (err) {
          logger.log(err);
        }
      }
    }
  });
}

module.exports.executeQurey = executeQurey;
