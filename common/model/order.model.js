//const dbService = require('../service/database.service');


module.exports = (sequelize, type) => {
    return sequelize.define('order', {
        ORDER_ID : {
          type : type.INTEGER,
          primaryKey : true,
          defaultValue: "nextval('DCA_ORDER_SEQ')"
          //autoIncrement : true
        },
        CREDIT_CHECK_STATUS : type.INTEGER,
        HOLD_REASON : type.STRING,
        LOGIN_ID : type.INTEGER,
        OOM_ORDER_ID : type.INTEGER,
        ORDER_DATE : type.DATE,
        ORDER_NUMBER : type.INTEGER,
        REASON_CODE_ID : type.INTEGER,
        REJECTION_REASON : type.STRING,
        STATUS_ID : type.INTEGER,
        SUBMISSION_TIME : type.DATE,
        AGENT_ID : type.INTEGER 
    },    
    {
        timestamps : false,
        tableName : 'DCA_ORDER'      
    })
}