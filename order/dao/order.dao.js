const script=require('../script/order.script');
const databaseDao=require('../../common/dao/database.dao');
const logger=require('../../common/util/logger.util');

module.exports.findByStatusIdAndAgentId=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      let query; 
      logger.info(`Order.dao : findByStatusIdAndAgentId : Started ` );
      query = script.findOrdersByStatusIdAndAgentIdScript(bind);
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query);
      logger.info(`Order.dao : findByStatusIdAndAgentId : End ` );
      resolve(result.rows);
  }catch(err){
    err.operationName='Order.dao';
    err.serviceName='findByStatusIdAndAgentId';
    reject(err)     
    }
  });
}

module.exports.addOrder=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.dao : findByStatusIdAndAgentId : Started ` );
      let query = script.findByStatusIdAndAgentIdScript(bind);
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query);
      logger.info(`Order.dao : findByStatusIdAndAgentId : End ` );
      resolve(result.rows);
  }catch(err){
    err.operationName='Order.dao';
    err.serviceName='findByStatusIdAndAgentId';
    reject(err)     
    }
  });
}


module.exports.findByStatusIdAndAgentIdPage=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.dao : findByStatusIdAndAgentIdPage : Started ` );
      let resultSize=parseInt(bind.limit,10)+parseInt(bind.offest,10)-1;
      let query = script.findByStatusIdAndAgentIdPageScript(bind.statusId,bind.agentId,bind.offest,
                                                 resultSize,bind.orderBy,bind.ordering );
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query);
      logger.info(`Order.dao : findByStatusIdAndAgentIdPage : End ` );
      resolve(result.rows);
  }catch(err){
    logger.error(`Order.dao : findByStatusIdAndAgentIdPage : error :: ${err}` );
    reject(`${err}`)     
    }
  }
  );
}
  
module.exports.getOrdersByOrderNumber=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.dao : openOrder : Started ` );
      let query = script.getOrdersByOrderNumber(bind);
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query);
      logger.info(`Order.dao : openOrder : End ` );
      resolve(result.rows);
  }catch(err){
    logger.error(`Order.dao : openOrder : error :: ${err}` );
    reject(`${err}`)     
    }
  }
  );
}

module.exports.getProductByOrderId=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.dao : openOrder : Started ` );
      let query = script.getProductByOrderId(bind);
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query);
      logger.info(`Order.dao : openOrder : End ` );
      resolve(result.rows);
  }catch(err){
    logger.error(`Order.dao : openOrder : error :: ${err}` );
    reject(`${err}`)     
    }
  }
  );
}
module.exports.getApprovedQuantity=(bind)=> {
  return new Promise(async (resolve, reject) => {
  try{
      logger.info(`Order.dao : getApprovedQuantity : Started ` );
      let query = script.getApprovedQuantity(bind);
      logger.info(`Query : ${query}`);
      const result = await databaseDao.executeQurey(query);
      logger.info(`Order.dao : getApprovedQuantity : End ` );
      resolve(result.rows);
  }catch(err){
    logger.error(`Order.dao : getApprovedQuantity : error :: ${err}` );
    reject(`${err}`)     
    }
  }
  );
}



module.exports.findAll =async function(){
  
    let query = script.findAll;
    const binds = {};
  
    const result = await databaseDao.executeQurey(query, binds);
    return result.rows;
  };


module.exports.findAll =async function(){

  let query = script.findAll;
  const binds = {};

  const result = await databaseDao.executeQurey(query, binds);
  return result.rows;
};
