const logger=require('../util/logger.util');

exports.handleError=function(err, req, res, next) {
    // add this line to include winston logging
    if(err.message ==undefined){
      logger.error(`${err.status || 500} - ${err} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
      res.locals.error = err;
      res.status(err.status || 500);
      res.send(err)    
 
    }else{
      logger.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);
      res.locals.message = err.message;
      res.locals.error = err;
      res.status(err.status || 500);
      res.send(err)    
      } 
  }; 


  
  